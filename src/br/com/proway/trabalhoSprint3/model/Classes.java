package br.com.proway.trabalhoSprint3.model;

import java.util.ArrayList;

public class Classes {
   private ArrayList<Aluno> classes;

    public ArrayList<Aluno> getClasses() {
        return classes;
    }

    public void setClasses(ArrayList<Aluno> classes) {
        this.classes = classes;
    }


}
