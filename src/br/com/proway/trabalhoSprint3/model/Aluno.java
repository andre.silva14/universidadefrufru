package br.com.proway.trabalhoSprint3.model;

import br.com.proway.trabalhoSprint3.interfaces.Media;

public class Aluno extends Pessoa implements Media {
    private int ra;
    private double nota1;
    private double nota2;
    private double media;

    public double getMediaA() {
        return media = media();
    }

    public Aluno(String nome, String cpf, int ra) {
        super(nome, cpf);
        this.ra = ra;
    }

    public int getRa() {
        return ra;
    }


    public void setNota1(double nota) {
        this.nota1 = nota;
    }


    public void setNota2(double nota) {
        this.nota2 = nota;
    }

    @Override
    public double media() {
        return (this.nota1 + this.nota2) / 2;

    }


}