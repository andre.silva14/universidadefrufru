package br.com.proway.trabalhoSprint3.model;

public class Professor extends Pessoa {
    private String materia;
    private double salario;


    public Professor(String nome, String cpf, String materia, double salario) {
        super(nome, cpf);
        this.materia = materia;
        this.salario = salario;
    }

    public void notas(Aluno ra, double nota1, double nota2) {
        ra.setNota1(nota1);
        ra.setNota2(nota2);
    }


    public String getMateria() {
        return materia;
    }


}
