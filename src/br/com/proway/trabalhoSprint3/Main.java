package br.com.proway.trabalhoSprint3;

import br.com.proway.trabalhoSprint3.model.Aluno;
import br.com.proway.trabalhoSprint3.model.Classes;
import br.com.proway.trabalhoSprint3.model.Professor;

import javax.swing.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Professor prof = new Professor("Bastião", "22444223", "Matemática", 5000);
        Classes classe1 = new Classes();
        classe1.setClasses(new ArrayList<>());

        for (int i = 0; i < 5; i++) {
            Aluno a = new Aluno("Fulano " + i, "988.456.676-7" + i, +1+i);
            classe1.getClasses().add(a);
            System.out.println(classe1.getClasses().get(i).getRa());
        }

        String texto = "";
        while (true) {
            texto = "Prof."+prof.getNome()+"   "+ prof.getMateria()+ "\n " +"       Turmas\n";
            for (int i = 0; i < classe1.getClasses().size(); i++) {
                texto += "RA: " + classe1.getClasses().get(i).getRa() + " Nome: " + classe1.getClasses().get(i).getNome() + "\n"
                        + "Nota: " + classe1.getClasses().get(i).getMediaA() + "\n";
            }


            String[] opcoes = {"Inserir Nota", "Sair"};

            int opcao = JOptionPane.showOptionDialog(null, texto, "Cadastro de aluno", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);

            if (opcao == 0) {
                int raAluno = Integer.parseInt(JOptionPane.showInputDialog("informe o RA do aluno: "));

                    double nota1 = Double.parseDouble(JOptionPane.showInputDialog("Informe a nota N1 do Aluno: "));
                    double nota2 = Double.parseDouble(JOptionPane.showInputDialog("Informe a nota N2 do Aluno: "));
                    Aluno a =  classe1.getClasses().get(raAluno);
                    prof.notas(a, nota1,nota2);
                    a.media();

            }else if (opcao != 0){
                break;
            }
        }
    }
}

